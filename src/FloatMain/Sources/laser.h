#ifndef LASER_H
#define LASER_H

unsigned int getDistance(void);
void set_laser_divisor(unsigned int);
void laser_set_samples(int); 
#endif LASER_H
