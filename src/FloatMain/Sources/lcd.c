/*!
   \file lcd.c
   \brief Implementation of LCD screen control for the Dragon12 board, as provided.
	 Optimised to reduce dependence on delays.
   \author Eduardo Nebot, adapted from Mazidi.
	 \author Optimisations by Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong,
	 6 June 2017.
   \date 28 March 2015
*/

//Displaying "HELLO" on LCD for Dragon12+ Trainer Board
//with HCS12 Serial Monitor Program installed. This code is for CodeWarrior IDE
//On Dragon12+ LCD data pins of D7-D4 are connected to Pk5-Pk2, En=Pk1,and RS=Pk0,
////Modified from Mazidi's book, Eduardo nebot  28/3/15
// included a loop to write some text
// need to get proper timing and make it more usable
//  COMWRT4(0x80) is comand for first line first character
// COMWRT4(0xC0)   is comand for second line first character
// need to evaluate proper delay

#include <hidef.h>      /* common defines and macros */
#include "derivative.h"      /* derivative-specific definitions */
#include "lcd.h"

//http://www.ee.nmt.edu/~rison/ee308_spr12/notes/Lecture_24.pdf
//details on how to optimise. It looks like the timing can be greatly reduced, and not all commands are necessary.

char LCDBuffer[32]; //!< Buffer to put new LCD output in.

/*!
   \brief Getter method for pointer to LCD output buffer.
   \return Pointer to LCD buffer.
*/
char * get_LCD_buf_ptr() {
  return LCDBuffer;
}

/*!
   \brief LCD initialisation
   \post LCD ready to be used, via LCD_put_string().
*/
void LCD_init() {
	DDRK = 0xFF;
	COMWRT4(0x33);  //reset sequence provided by data sheet
	MSDelay(1);
	COMWRT4(0x32);  //reset sequence provided by data sheet
	MSDelay(1);
	COMWRT4(0x28);	//Function set to four bit data length
	              	//2 line, 5 x 7 dot format
	MSDelay(1);
	COMWRT4(0x06);  //entry mode set, increment, no shift
	MSDelay(1);
	COMWRT4(0x0E);  //Display set, disp on, cursor on, blink off
	MSDelay(1);
	COMWRT4(0x01);  //Clear display
	MSDelay(1);
	COMWRT4(0x80);  //set start posistion, home position
	MSDelay(1);
}

/*!
   \brief Sets LCD to display a string.
   \param string Pointer to string, maximum length of 32 bytes (16 per line of the LCD).
   \pre LCD must be initialised.
   \post LCD text updated.
*/
void LCD_put_string(char * string) {
  int i;
  LCD_init();
  for(i = 0; i < 16; i++) {
      if(!string[i]) return;
      DATWRT4((unsigned char)string[i]);
  }
  COMWRT4(0xC0);  //set start posistion, home position
  MSDelay(1);
  for(i = 16; i < 32; i++) {
    if(!string[i]) return;
    DATWRT4((unsigned char)string[i]);
  }
}

void COMWRT4(unsigned char command)  {
  unsigned char x;

  x = (command & 0xF0) >> 2;         //shift high nibble to center of byte for Pk5-Pk2
  LCD_DATA =LCD_DATA & ~0x3C;          //clear bits Pk5-Pk2
  LCD_DATA = LCD_DATA | x;          //sends high nibble to PORTK
  MSDelay(1);
  LCD_CTRL = LCD_CTRL & ~RS;         //set RS to command (RS=0)
  MSDelay(1);
  LCD_CTRL = LCD_CTRL | EN;          //rais enable
  MSDelay(5);
  LCD_CTRL = LCD_CTRL & ~EN;         //Drop enable to capture command
  MSDelay(15);                       //wait
  x = (command & 0x0F)<< 2;          // shift low nibble to center of byte for Pk5-Pk2
  LCD_DATA =LCD_DATA & ~0x3C;         //clear bits Pk5-Pk2
  LCD_DATA =LCD_DATA | x;             //send low nibble to PORTK
  LCD_CTRL = LCD_CTRL | EN;          //rais enable
  MSDelay(5);
  LCD_CTRL = LCD_CTRL & ~EN;         //drop enable to capture command
  MSDelay(15);
}

void DATWRT4(unsigned char data)  {
  unsigned char x;
  x = (data & 0xF0) >> 2;
  LCD_DATA =LCD_DATA & ~0x3C;
  LCD_DATA = LCD_DATA | x;
  MSDelay(1);
  LCD_CTRL = LCD_CTRL | RS;
  MSDelay(1);
  LCD_CTRL = LCD_CTRL | EN;
  MSDelay(1);
  LCD_CTRL = LCD_CTRL & ~EN;
  MSDelay(5);

  x = (data & 0x0F)<< 2;
  LCD_DATA =LCD_DATA & ~0x3C;
  LCD_DATA = LCD_DATA | x;
  LCD_CTRL = LCD_CTRL | EN;
  MSDelay(1);
  LCD_CTRL = LCD_CTRL & ~EN;
  MSDelay(15);
}

/*!
   \brief Delay function to waste cycles. Note, reduced to 0.1ms to speed up LCD updates.
   \param itime Number of 0.1ms periods to delay.
*/
 void MSDelay(unsigned int itime)
  {
    unsigned int i; unsigned int j;
    for(i=0;i<itime;i++)
     for(j=0;j<100;j++);  // NOTE: this used to be j<1000, now less -> 0.1ms
 }
