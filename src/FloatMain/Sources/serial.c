/*!
   \file serial.c
   \brief Serial interface and control.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong.
   \date 6 June 2017
*/

#include "derivative.h"
#include "serial.h"
#include <stdio.h>
#include "laser.h"
#include <math.h>
#include "timer.h"
#include <stdlib.h>
#include "servo.h"
#include <stdarg.h>
#include <string.h>

char outBuffer[BUFFER_LENGTH];	//!< Buffer for serial output (to PC).
char inBuffer[BUFFER_LENGTH];		//!< Buffer to store input (from PC).
char commandReady;							//!< High when complete command received, else low.
char inSerialConfigMode;				//!< High when configuration permitted, else low.
char buffer[50];								//!< General use buffer for LCD output.

char * outBufRd;	//!< Pointer to next read location in output buffer.
char * outBufWt;	//!< Pointer to next write location in output buffer.
char * inBufRd;		//!< Pointer to next read location in input buffer.
char * inBufWt;		//!< Pointer to next write location in input buffer.

/*!
   \brief Setter for serial config mode (on).
   \post Configuration commands now permitted on serial interface.
*/
void enter_serial_config() {
    inSerialConfigMode = 1;
}

/*!
   \brief Setter for serial config mode (off).
   \post Configuration commands now permitted on serial interface.
*/
void exit_serial_config() {
    inSerialConfigMode = 0;
}

/*!
   \brief Getter for serial output buffer.
*/
char * get_serial_buf_ptr() {
    return buffer;
}

/*!
   \brief Print a formated string over serial
   \param format Formatted string to send.
	 \param ... Arguments for any string placeholder values in format.
	 \note Buffer used to send is 50 bytes long, do not pass parameters that generate more.
   \post String sent over serial connection.
*/
void serial_printf(const char * format, ...) {
    va_list argPtr;
    va_start(argPtr, format);
    vsprintf(&buffer, format, argPtr);
    serial_tx_str_vp((void*)&buffer);
    va_end(argPtr);
}

/*!
   \brief Print a formated string to the LCD
   \param format Formatted string to display.
	 \param ... Arguments for any string placeholder values in format.
	 \note Buffer used to send is 50 bytes long, do not pass parameters that generate more.
	 LCD is a 16x2 display; only first 32 bytes of string will be displayed.
   \post String sent over serial connection.
*/
//print a formatted string to the LCD - using a buffer of length 50 - only the first 32 characters will be displayed
void LCD_printf(const char * format, ...) {
    va_list argPtr;
    va_start(argPtr, format);
    vsprintf(&buffer, format, argPtr);
    LCD_put_string(&buffer);
    va_end(argPtr);
}

/*!
   \brief Initialises serial (SCI) interface for sending and receiving.
   \pre Physical hardware connection should exist with PC on RS232 port.
   \post Serial communication established at 19200 baud.
*/
void initSCI(void) {
  SCI1BDH = 0x0;
  SCI1BDL = 78; // 19200 Baud Rate (9C)
  SCI1CR1 = 0x0;
  SCI1CR2 = 0x2C; // Enable interrupts on RDRF, Receiver, Transmitter
}

/*!
   \brief Initialises variables and SCI interface.
   \post Variables set to default values and serial interface initialised.
*/
void init_command(void) {
    outBufWt = outBuffer;
    outBufRd = outBuffer;
    inBufRd = inBuffer;
    inBufWt = inBuffer;
    commandReady = FALSE;
    initSCI();
}

/*!
   \brief Gets next char from circular input buffer. Shorthand for dereference
	 and increment inBufRd.
   \post inBufRd incremented to next position, minding buffer circularity.
   \return The character pointed to by inBufRd, before the function was called.
	 \note Equivalent to *inBufRd++, but ensures buffer is circular.
*/
char diib() {
    char let;
    let = *inBufRd;
    INC(inBufRd, inBuffer);
    return let;
}

/*!
   \brief Send string over serial interface.
   \param voidP Pointer to string, internally treated as char*.
   \pre Serial link should be initialised, by calling init_command().
   \post Buffer pointers changed, string sent over serial.
*/
void serial_tx_str_vp(void * voidP) {
    char * str;
    str = (char*)voidP;
    while(*str != 0) {
        *outBufWt = *str++;
        INC(outBufWt, outBuffer);
    }
    *outBufWt = '_';
    INC(outBufWt, outBuffer);
    SCI1CR2 |= 0x80; //todo : not sure how serial works, is this necessary to get transmit interrupts?
}

/*!
   \brief Serial command to print text on LCD display.
   \pre String to print must exist in input buffer.
   \post LCD text changed, Buffer pointers changed.
*/
void d_disp_on_LCD() {
    unsigned int i;
    char lcdMsg[32];
    char * msgPtr;
    diib();
    for(i = 0; i < 32; i++) lcdMsg[i] = 0; //zero out the buffer so the last message doesn't show
    msgPtr = lcdMsg;
    while(*inBufRd != '_') {
        *msgPtr++ = *inBufRd;
        diib();
    }
    diib();
    LCD_put_string(lcdMsg);
}

/*!
   \brief Serial command to send current LIDAR range over serial.
   \pre As getDistance() in laser.c; serial link must be initialised.
   \post Buffer pointers changed, string sent over serial.
*/
void r_send_laser_dist_over_serial() {
    unsigned int distance = getDistance();
    serial_printf("R(%u)", distance);
    diib();
    diib();
}

/*!
   \brief Serial command to send current LIDAR range to LCD.
   \pre As getDistance() in laser.c; LCD must be initialised.
   \post Buffer pointers changed, LCD text changed.
*/
void R_send_laser_dist_to_LCD() {
    unsigned int distance = getDistance();
    LCD_printf("R(%u)", distance);
    diib();
    diib();
}

/*!
   \brief Serial command to echo string back over serial.
   \pre String must exist at inBufRd; serial link must be initialised.
   \post Buffer pointers changed, string sent over serial.
*/
void i_echo_message_over_serial() {
    char * chPtr;
    diib();
    chPtr = inBufRd; //point to first character in string

    // Replace the _ in the inbuffer with a null terminator, so that a pointer to
		// the inBuffer can be used as an argument.
    while(*inBufRd != '_') diib();
    *inBufRd = 0;
    diib(); //move past null terminator
    serial_tx_str_vp((void*)chPtr);
}

/*!
   \brief Serial command to set PTU pan angle.
   \pre inBufRd must point to a signed decimal value in centidegrees.
   \post inBufRd changed.
*/
void p_set_pan_angle() {
    char sign;
    int angle;
    diib();
    if(*inBufRd == '-') {
        sign = -1;
        diib();
    }
    else sign = 1;
    angle = 0;
    while(*inBufRd != '_') {
        angle *= 10;
        angle += *inBufRd - '0';
        diib();
    }
    diib();
    setPanAngle(sign * angle);
}

/*!
   \brief Serial command to set PTU tilt angle.
   \pre inBufRd must point to a signed decimal value in centidegrees.
   \post inBufRd changed.
*/
void t_set_tilt_angle() {
    char sign;
    int angle;
    diib();
    if(*inBufRd == '-') {
        sign = -1;
        diib();
    }
    else sign = 1;
    angle = 0;
    while(*inBufRd != '_') {
        angle *= 10;
        angle += *inBufRd - '0';
        diib();
    }
    diib();
    setTiltAngle(sign * angle);
}

/*!
   \brief Debug method to display char on LEDs.
   \pre inBufRd must point to two chars, to be displayed on high and low nibble
	 of LEDs respectively.
	 \pre LEDs must be configured for output.
   \post inBufRd incremented, LED output changed.
*/
void l_disp_on_LEDs() {
    char chr;
    chr = 0;
    diib();
    if(*inBufRd >= '0' && *inBufRd <= '9') chr += 16 * (*inBufRd - '0');
    else chr += 16 * (*inBufRd - 'A' + 10);
    diib();
    if(*inBufRd >= '0' && *inBufRd <= '9') chr += (*inBufRd - '0');
    else chr += (*inBufRd - 'A' + 10);
    PORTB = chr;
    diib();
    diib();
}

/*!
   \brief Serial command to send current pan, tilt, and LIDAR range over serial.
   \pre As getDistance() in laser.c; serial link must be initialised.
   \post Buffer pointers changed, string sent over serial.
*/
void angles_range_over_serial() {
    char buf[25];
    int pan;
    int tilt;
    int range;

    range = getDistance();
    pan = getPanAngle();
    tilt = getTiltAngle();

    serial_printf("P(%d, %d, %d)", pan, tilt, range);
    // serial_tx_str_vp((void*)&buf);
}

/*!
   \brief Serial command to send current raw accelerometer data over serial.
   \pre IIC and serial link must be initialised.
   \post Buffer pointers changed, string sent over serial.
*/
void a_accelerometer_over_serial() {
    int axraw;
    int ayraw;
    int azraw;
    char buf[25];
    adxl345_getrawdata(&axraw, &ayraw, &azraw);
    sprintf(buf, "A(%d, %d, %d)", axraw, ayraw, azraw);
    serial_tx_str_vp((void*)&buf);
    diib();
    diib();
}

/*!
   \brief Serial command to send current raw accelerometer data to LCD.
   \pre IIC and LCD must be initialised.
   \post Buffer pointers changed, LCD text changed.
*/
void A_accelerometer_to_LCD() {
    int axraw;
    int ayraw;
    int azraw;
    char buf[25];
    adxl345_getrawdata(&axraw, &ayraw, &azraw);
    sprintf(buf, "A(%d, %d, %d)", axraw, ayraw, azraw);
    LCD_put_string((void*)&buf);
    diib();
    diib();
}

/*!
   \brief Serial command to send current raw gyroscope data over serial.
   \pre IIC and serial link must be initialised.
   \post Buffer pointers changed, string sent over serial.
*/
void g_gyro_over_serial() {
    int axraw;
    int ayraw;
    int azraw;
    char buf[25];
    l3g4200d_getrawdata(&axraw, &ayraw, &azraw);
    sprintf(buf, "G(%d, %d, %d)", axraw, ayraw, azraw);
    serial_tx_str_vp((void*)&buf);
    diib();
    diib();
}

/*!
   \brief Serial command to send current raw gyroscope data to LCD.
   \pre IIC and LCD must be initialised.
   \post Buffer pointers changed, LCD text changed.
*/
void G_gyro_to_LCD() {
    int axraw;
    int ayraw;
    int azraw;
    char buf[25];
    l3g4200d_getrawdata(&axraw, &ayraw, &azraw);
    sprintf(buf, "G(%d, %d, %d)", axraw, ayraw, azraw);
    LCD_put_string((void*)&buf);
    diib();
    diib();
}

/*!
   \brief Serial command to send current raw magnetometer data over serial.
   \pre IIC and serial link must be initialised.
   \post Buffer pointers changed, string sent over serial.
*/
void m_magno_over_serial() {
    int axraw;
    int ayraw;
    int azraw;
    char buf[25];
    hm5883_getrawdata(&axraw, &ayraw, &azraw);
    sprintf(buf, "M(%d, %d, %d)", axraw, ayraw, azraw);
    serial_tx_str_vp((void*)&buf);
    diib();
    diib();
}

/*!
   \brief Serial command to send current raw magnetometer data to LCD.
   \pre IIC and LCD must be initialised.
   \post Buffer pointers changed, LCD text changed.
*/
void M_magno_to_LCD() {
    int axraw;
    int ayraw;
    int azraw;
    char buf[25];
    hm5883_getrawdata(&axraw, &ayraw, &azraw);
    sprintf(buf, "M(%d, %d, %d)", axraw, ayraw, azraw);
    LCD_put_string((void*)&buf);
    diib();
    diib();
}

/*!
   \brief Serial command to send current tilt elevation over serial,
	 from accelerometer data.
   \pre IIC and serial link must be initialised.
   \post Buffer pointers changed, string sent over serial.
*/
void e_elevation_over_serial() {
    int axraw;
    int ayraw;
    int azraw;
    float elev;

    adxl345_getrawdata(&axraw, &ayraw, &azraw);
    elev = atan(((float)azraw)/((float)axraw)) * ONE_RADIAN_IN_DEGREES;
    sprintf(buffer, "E(%f)", elev);
    serial_tx_str_vp((void*)buffer);
    diib();
    diib();
}

/*!
   \brief Serial command to send current tilt elevation to LCD, from accelerometer data.
   \pre IIC and LCD must be initialised.
   \post Buffer pointers changed, LCD text changed.
*/
void E_elevation_to_LCD() {
    int axraw;
    int ayraw;
    int azraw;
    float elev;

    adxl345_getrawdata(&axraw, &ayraw, &azraw);
    elev = atan(((float)azraw)/((float)axraw)) * ONE_RADIAN_IN_DEGREES;
    sprintf(buffer, "E(%f)", elev);
    LCD_put_string(buffer);
    diib();
    diib();
}

/*!
   \brief Serial command to send current system time over serial.
   \pre System time/clock must be initialised, see timer.c.
   \post Buffer pointers changed, string sent over serial.
*/
void c_sysTime_over_serial(void * ptr) {
    unsigned long int t;
    t = getSysTime();
    serial_printf("C(%lu)", t);
    diib();
    diib();
}

/*!
   \brief Serial command to send current system time to LCD.
   \pre System time/clock must be initialised, see timer.c.
   \post Buffer pointers changed, LCD text changed.
*/
void C_sysTime_to_LCD(void * ptr) {
    unsigned long int t;
    t = getSysTime();
    LCD_printf("C(%lu)", t);
    diib();
    diib();
}

/*!
   \brief Parses a string into an integer.
   \pre String representing signed int must be pointed to by inBufRd.
   \post Buffer pointers changed.
   \return Signed int as parsed.
*/
int parse_int_from_inbuf() {
    char sign;
    int result;
    if(*inBufRd == '-') {
        sign = -1;
        diib();
    }
    else {
        sign = 1;
    }
    result = 0;
    while(*inBufRd != '_') {
        result *= 10;
        result += *inBufRd - '0';
        diib();
    }
    diib();
    return result * sign;
}

/*!
   \brief Parse configuration commands from serial.
   \pre Valid configuration command must be pointed to by inBufRd.
   \post Buffer pointer changed, configuration settings changed accordingly.
*/
void f_configure() {
    char let;
    if(!inSerialConfigMode) {
        //skip through any config instructions
        while(*inBufRd != '_') {
            diib();
        }
        diib();
        return;
    }
    diib();
    switch(*inBufRd) {
        case 'r': //config laser resolution
            diib();
            set_laser_resolution(parse_int_from_inbuf());
            break;
        case 's': //config scan type
            diib();
            if(*inBufRd == 'r') set_scan_type(SCAN_TYPE_RECT);
            else if(*inBufRd == 'p') set_scan_type(SCAN_TYPE_POLY);
            diib();
            diib();
            break;
        case 'b' : //configure Bounds
            diib();
            switch(*inBufRd) {
                case 'l' :
                  diib();
                  set_left_bound(parse_int_from_inbuf() * 100);
                  break;
                case 'r' :
                  diib();
                  set_right_bound(parse_int_from_inbuf() * 100); break;
                case 't' :
                  diib();
                    set_top_bound(parse_int_from_inbuf() * 100); break;
                case 'b' :
                  diib();
                  set_bottom_bound(parse_int_from_inbuf() * 100); break;
            }
            //no increment needed here - parse int assumes there is a _ after the number and moves past it
            break;
        case 'f': //configure laser freqency
            diib();
            set_laser_freq_divisor(parse_int_from_inbuf());
            break;
        case 'n' : //configure number of laser samples per position
            diib();
            set_laser_sample_num(parse_int_from_inbuf());
            break;
    }
}

/*!
   \brief Serial command to start scan procedure.
   \pre Configuration must be complete before calling, as will be disabled.
   \post Selected scan procedure scheduled to begin.
*/
void s_start_scan() {
    exit_serial_config();
    if(get_scan_type() == SCAN_TYPE_RECT) {
        start_fast_scan(NULL);
    }
    else {
        start_poly_scan();
    }
    diib();
}

/*!
   \brief Dispatch table to call correct function based on command input.
   \param ptr For compatibility reasons, pass a null pointer.
   \pre inBufRd must point to a valid command character, followed by data required
	 by the specified command (if any).
*/
void do_input_command(void * ptr) {

    switch(*inBufRd) {
        case 'd' : d_disp_on_LCD(); break;
        case 'r' : r_send_laser_dist_over_serial(); break;
        case 'R' : R_send_laser_dist_to_LCD(); break;
        case 'a' : a_accelerometer_over_serial(); break;
        case 'A' : A_accelerometer_to_LCD(); break;
        case 'g' : g_gyro_over_serial(); break;
        case 'G' : G_gyro_to_LCD(); break;
        case 'E' : E_elevation_to_LCD(); break;
        case 'e' : e_elevation_over_serial(); break;
        case 'm' : m_magno_over_serial(); break;
        case 'M' : M_magno_to_LCD(); break;
        case 'i' : i_echo_message_over_serial(); break;
        case 'p' : p_set_pan_angle(); break;
        case 't' : t_set_tilt_angle(); break;
        case 'l' : l_disp_on_LEDs(); break;
        case 'c' : c_sysTime_over_serial(NULL); break;
        case 'C' : C_sysTime_to_LCD(NULL); break;
        case 'f': f_configure(); break;
        case 's' : s_start_scan(); break;
        //case 'c' : c_sent_global_counter_as_raw_int(); break;
        default :
            diib();
            return;
    }
    commandReady = FALSE;
}

/*!
   \brief Interrupt Service Routine initiated by the SCI1 interrupt
   \post If new data being received, it is added to the input buffer.
	 \post If data is ready to be sent, the next character on the output buffer is
	 sent.
   \details This interrupt is implemented to give interrupt driven input and output.
	 When an RDRF interrupt is received, the data is copied to the input buffer, and
	 processed later to clear this interrupt and prepare for the next received char.
	 When a TDRE interrupt is received, the next output char is placed on the data
	 register and we return from interrupt, so that no cycles are wasted waiting to
	 send the next byte.
*/
interrupt 21 void SCI1_ISR(void) {
    char in;
    if (SCI1SR1 & SCI1SR1_RDRF_MASK) { // If this is an receiver (RDRF) interrupt,
      in = SCI1DRL & 0b01111111; //dodgy work around because python seems to send the wrong thing sometimes
      *inBufWt = in;
      INC(inBufWt, inBuffer);

      if(in == '_') {
          commandReady = TRUE;
          // workQueue[prodInd] = do_input_command;
          // INCW(prodInd);
      }
      SCI1CR2 |= 0x80;                 //   interrupt, to start/continue sending data
    }
    if (SCI1SR1 & SCI1SR1_TDRE_MASK) { // If this is a ready-to-transmit (TDRE) interrupt,
        if(outBufRd != outBufWt) {
            SCI1DRL = *outBufRd;
            INC(outBufRd, outBuffer);
        }
        else {
            SCI1CR2 &= 0x7F; //stop receiving transmist interrupts
        }
    }
}
