/*!
   \file config.c
   \brief Onboard configuration routines, to be executed after startup to allow
	 				for user control
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong
   \date 6 June 2017

	 This file implements functions each asking the user a configuration question,
	 displaying the current value and responding to instructions to increment,
	 decrement, or confirm the current value. All are called directly from config(),
	 allowing all configuration to be retrieved at once from main.

	 This file is implemented through methods that call setter functions the scan
	 implementation files (newScan.c and polyscan.c) to configure the appropriate
	 variables. As such, these methods can also be called from the serial interface
	 when user interface on the LCD is not required.
*/

#include "config.h"
#include "keyboard.h"
#include "lcd.h"
#include "servo.h"
#include "polyscan.h"
#include "newScan.h"
#include "serial.h"

#include <stdio.h>


char output[32];//!< Buffer in which to store strings for LCD output.
int min, max;		//!< Max and min allowable values for each question.
int current;		//!< Current value at each question.
char scanType;	//!< Store for type of scan selected.

/*!
   \brief Initialises default configuration when user configuration is not
	 desired.
*/
void init_defaults() {
	set_scan_type(SCAN_TYPE_RECT);
	set_laser_freq_divisor(1);
	set_right_bound(5000);
	set_left_bound(-5000);
	set_top_bound(2500);
	set_bottom_bound(-2500);
	polyScan_init();
	newScan_init();
	set_laser_sample_num(0);
}

/*!
   \brief Sets the type of scan to occur when a scan begins.
   \param type Either of SCAN_TYPE_RECT or SCAN_TYPE_POLY.
   \post scanType changed to reflect parameter.
*/
void set_scan_type(char type) {
	scanType = type;
}

/*!
   \brief Returns the currently set scan type.
   \pre Scan type must have been previously set with set_scan_type, else result
	 is undefined.
   \return One of SCAN_TYPE_RECT or SCAN_TYPE_POLY.
*/
char get_scan_type() {
	return scanType;
}

/*!
   \brief Main configuration routine, which calls each configuration question
	 in turn.
   \pre All hardware, including servo motors, LIDAR, IMU, etc. should be
	 initialised prior to calling, as a scan may be scheduled to begin.
   \post Depending on options selected, a scan may be scheduled to begin after
	 return. If no scan is scheduled, configuration set methods and scan initialisation
	 must be called from elsewhere, e.g. init_defaults() or from serial interface.
*/
void config() {
	init_keyboard();

	// Provides user instructions
	introduction();

	// If user selects serial setup, return.
	if(config_interface() == CONFIG_SERIAL) {
		LCD_put_string("Serial selected.Continue on PC.");
		return;
	}

	// Otherwise get remainder of settings from user.
	config_scan_type();
	config_left_bound();
	config_right_bound();
	config_top_bound();
	config_bottom_bound();
	config_resolution();
	config_samples();
	config_frequency();
	config_ready();
}

/*!
   \brief Sets left angle bound for scan routines.
   \param b The leftmost angle limit in hundredths of one degree (centidegrees).
*/
void set_left_bound(int b) {
	set_rect_left_bound(b);
	set_poly_left_bound(b);
}

/*!
   \brief Sets right angle bound for scan routines.
   \param b The rightmost angle limit in hundredths of one degree (centidegrees).
*/
void set_right_bound(int b) {
	set_rect_right_bound(b);
	set_poly_right_bound(b);
}

/*!
   \brief Sets top angle bound for scan routines.
   \param b The upper angle limit in hundredths of one degree (centidegrees).
*/
void set_top_bound(int b) {
	set_rect_top_bound(b);
	set_poly_top_bound(b);
}

/*!
   \brief Sets bottom angle bound for scan routines.
   \param b The lower angle limit in hundredths of one degree (centidegrees).
*/
void set_bottom_bound(int b) {
	set_rect_bottom_bound(b);
	set_poly_bottom_bound(b);
}

/*!
   \brief Sets the laser frequency divisor
   \param i The divisor by which laser frequency should be scaled from 300Hz.
*/
void set_laser_freq_divisor(volatile unsigned int i) {
	set_laser_divisor(i);
}

/*!
   \brief Sets the laser samples per orientation
   \param i The number of samples to take per orientation.
*/
void set_laser_sample_num(int i) {
	laser_set_samples(i);
	set_num_samples(i);
	set_rect_laser_samples(i);
}

/*!
   \brief Sets the resolution of the scan procedures.
   \param i The resolution in hundredths of a degree (centidegrees).
*/
void set_laser_resolution(int i) {
	set_poly_res(i);
	set_rect_res(i);
}

/*!
   \brief Welcomes the user to the device and provides instructions for
	 interacting with the device.
	 \post LCD text changed.
*/
void introduction() {
	LCD_put_string("Welcome. A++,B--D to Continue");
	// Wait for user to press D/Enter to confirm.
	while(get_next_key() != ENTER);
}

/*!
   \brief Allows user to select interface for subsequent configuration: onboard
	 LCD (Local) or from a connected PC using the serial interface.
	 \post LCD text changed.
   \return One of CONFIG_LOCAL or CONFIG_SERIAL.
*/
char config_interface() {
  const char* local = "Local";
  const char* serial = "Serial";

	//Only two options
  min = 0;
  max = 1;
  current = 0;

	// Allow input forever, until confirmation.
  while(1) {
    sprintf(&output, "Config type:    %s", (current==CONFIG_LOCAL)? local : serial);
    LCD_put_string(&output);

   switch (get_next_key()) {
			case INCREMENT:
			case DECREMENT:
				current = !current; // Toggled by either key.
				break;
			case ENTER:
				return current;
		}
  }
}

/*!
   \brief Allows user to select type of scan to be conducted.
   \post Updates scanType accordingly.
	 \post LCD text changed.
*/
void config_scan_type() {
  const char* rect = "Rectangle";
  const char* poly = "Polygon";

	//Two options
  min = 0;
  max = 1;
  current = 0;

  while(1) {
		sprintf(&output, "Object type:    %s", (current==SCAN_TYPE_RECT) ? rect : poly);
		LCD_put_string(&output);

		switch (get_next_key()) {
			case INCREMENT:
			case DECREMENT:
				current = !current; // Toggled by either key.
				break;
			case ENTER:
			  set_scan_type((current==SCAN_TYPE_RECT) ? SCAN_TYPE_RECT : SCAN_TYPE_POLY);
				return;
		}
  }
}

/*!
   \brief Allows user to select left angle bound for scan.
   \post Left bound updated.
	 \post LCD text changed.
	 \post PTU orientation changed.
*/
void config_left_bound() {
	// Range of options, default is far left.
	min = -50;
	max = 49;
	current = -50;

	while(1) {
		sprintf(&output, "Left bound:     %+i deg", current);
		LCD_put_string(&output);

		setPanAngle(100*current); // Move with input

		switch (get_next_key()) {
			case INCREMENT:
				current += ((current == max) ? 0 : 1); // ++ unless max
				break;
			case DECREMENT:
				current -= ((current == min) ? 0 : 1); // --- unless min
				break;
			case ENTER:
				set_left_bound(current*100); // *100 for centiDegrees
				min = current+1;
				return;
		}
	}
}

/*!
   \brief Allows user to select right angle bound for scan.
	 \pre config_left_bound() must have already been called, or min set to a valid value.
   \post Right bound updated.
	 \post LCD text changed.
	 \post PTU orientation changed.
*/
void config_right_bound() {
	// Range of options, min set by config_left_bound()
	max = 50;
	current = 50;

	while(1) {
		sprintf(&output, "Right bound:    %+i deg", current);
		LCD_put_string(&output);

		setPanAngle(100*current);

		switch (get_next_key()) {
			case INCREMENT:
				current += ((current == max) ? 0 : 1);
				break;
			case DECREMENT:
				current -= ((current == min) ? 0 : 1);
				break;
			case ENTER:
				set_right_bound(current*100);
				setPanAngle((min-1+current)/2*100); // Go to middle
				return;
		}
	}
}

/*!
   \brief Allows user to select top angle bound for scan.
   \post Top bound updated.
	 \post LCD text changed.
	 \post PTU orientation changed.
*/
void config_top_bound() {
	// Range, default is top
	min = -24;
	max = 25;
	current = 25;

	while(1) {
		sprintf(&output, "Top bound:      %+i deg", current);
		LCD_put_string(&output);

		setTiltAngle(100*current);

		switch (get_next_key()) {
			case INCREMENT:
				current += ((current == max) ? 0 : 1);
				break;
			case DECREMENT:
				current -= ((current == min) ? 0 : 1);
				break;
			case ENTER:
				set_top_bound(current*100);
				max = current-1;
				return;
		}
	}
}

/*!
   \brief Allows user to select bottom angle bound for scan.
	 \pre config_top_bound() must have already been called, or max set to a valid value.
   \post Bottom bound updated.
	 \post LCD text changed.
	 \post PTU orientation changed.
*/
void config_bottom_bound() {
	// Range, max set by config_top_bound()
	min = -25;
	current = -25;

	while(1) {
		sprintf(&output, "Bottom bound:   %+i deg", current);
		LCD_put_string(&output);

		setTiltAngle(100*current);

		switch (get_next_key()) {
			case INCREMENT:
				current += ((current == max) ? 0 : 1);
				break;
			case DECREMENT:
				current -= ((current == min) ? 0 : 1);
				break;
			case ENTER:
				set_bottom_bound(current*100);
				setTiltAngle((max+1+current)/2*100); // Go to middle
				return;
		}
	}
}

/*!
   \brief Allows user to select scan resolution.
   \post Scan resolution updated.
	 \post LCD text changed.
*/
void config_resolution() {
	extern int resolution;

	//Range, default is optimum
	min = 10;
	max = 1000;
	current = 50;

	while(1) {
		sprintf(&output, "Scan resolution:%+3.1f deg", (float)((float)current/100.0));
		LCD_put_string(&output);

		switch (get_next_key()) {
			case INCREMENT:
				current += ((current == max) ? 0 : 10);
				break;
			case DECREMENT:
				current -= ((current == min) ? 0 : 10);
				break;
			case ENTER:
				set_laser_resolution(current);
				return;
		}
	}
}

/*!
   \brief Allows user to select number of laser samples per orientation.
   \post Samples per orientation store updated.
	 \post LCD text changed.
*/
void config_samples() {
  //extern char samples;
  min = 1;
  max = 1000;
  current = 1;

  while(1) {
		sprintf(&output, "Samples/point:  %+i samples", current);
		LCD_put_string(&output);

		switch (get_next_key()) {
			case INCREMENT:
				current += ((current == max) ? 0 : 1);
				break;
			case DECREMENT:
				current -= ((current == min) ? 0 : 1);
				break;
			case ENTER:
				//samples = current;
				return;
		}
	}
}

/*!
   \brief Allows user to select laser frequency.
   \post laser frequency divisor updated.
	 \post LCD text changed.
*/
void config_frequency() {
  extern unsigned int laserFreqDivisor;
  unsigned int newDivisor, loopCounter;
  min = 0;
  max = 6;
  current = 0;

  while(1) {

    newDivisor = 1;
    for (loopCounter = 0; loopCounter<current; loopCounter++) newDivisor *= 3;

		sprintf(&output, "LIDAR frequency:%3.1f Hz", ((float)300/(float)newDivisor) );
		LCD_put_string(&output);

		laserFreqDivisor = newDivisor;

		switch (get_next_key()) {
			case INCREMENT:
				current -= ((current == min) ? 0 : 1);
				break;
			case DECREMENT:
				current += ((current == max) ? 0 : 1);
				break;
			case ENTER:
				return;
		}
  }
}

/*!
   \brief Waits for user confirmation before beginning scan routine.
   \post Scan routine scheduled to begin.
	 \post LCD text changed.
*/
void config_ready() {
	LCD_put_string("Ready. D to     start...");

	// Wait for confirmation to start...
	while(get_next_key() != ENTER);

	serial_tx_str_vp('BEGIN SCAN');
  if(get_scan_type() == SCAN_TYPE_RECT) {
        start_fast_scan(NULL);
    }
    else {
        start_poly_scan();
    }
}
