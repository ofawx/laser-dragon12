#ifndef TIMER_H
#define TIMER_H

void initTimer();
unsigned long int getSysTime();
void do_after(unsigned int millis, void (*func)(void*), void * data);

#endif // TIMER_H
