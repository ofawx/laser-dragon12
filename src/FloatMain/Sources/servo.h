#ifndef SERVO_H
#define SERVO_H

typedef enum {PAN, TILT} Servo;
#define TILT_PWM PWMDTY45
#define PAN_PWM PWMDTY67

/* DO NOT MODIFY & RUN ON PTU UNTIL VERIFIED WITH SCOPE */
#define PERIOD_CYCLES 60000
// Formula: (0.9+(((anglefrom0to180)/180)*1.2))/20*60000
#define LEFT 3500
#define RIGHT 5500
#define CENTER 4500
#define	UP 5000
#define DOWN 4000
#define SAFE_MIN 2400
#define SAFE_MAX 6600
// #define SAFE_MIN 2750
// #define SAFE_MAX 6250


// Public methods
// All angles in centiDegrees for ease, pwm output resolution = 5 centiDegrees -> rounded toward 0 to next %5==0
void initServo();
void setPanAngle(int centiDegrees);
void setTiltAngle(int centiDegrees);
void incrementPanAngle(int centiDegrees);
void incrementTiltAngle(int centiDegrees);
signed int getPanAngle();
signed int getTiltAngle();

void start_disp_servo_LCD();
void stop_disp_servo_LCD();
void disp_servo_LCD();
void send_az_el_to_PC(); 

// Internal methods, avoid use
unsigned char pwmSafe(unsigned int value);
void setPanPWM(unsigned int value);
void setTiltPWM(unsigned int value);
signed int angleFrom(unsigned int pwm);
unsigned int pwmFrom(int angle);
unsigned int getPanPWM();
unsigned int getTiltPWM();

#endif //SERVO_H
