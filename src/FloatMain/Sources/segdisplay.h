#ifndef SEGDISPLAY_H
#define SEGDISPLAY_H

void initSegDisplay();
void displaySeg(unsigned int newvalue);
void setDecimalPoints(unsigned char first, unsigned char second);

#endif //SEGDISPLAY_H