/*!
   \file main.c
   \brief Entry point for program, and implementation of looping work queue.
   \author Oscar Fawkes, Tristan Heywood, Tom McFadyen, Emily Zhong.
	 \author Work queue implemented by Tristan Heywood.
   \date 6 June 2017.
	 \details At the core of this system is a 'work queue'. This is effectively a circular
	 buffer of function pointers. In addition there is a circular buffer of void
	 pointers and one of long unsigned ints (delays). If one functions wants another
	 function to be called at a later time (for example if the original function is
	 an interrupt), it can add a pointer to that function to the work queue and a
	 pointer to the data it wants to send to that function to the data queue (typecast
	 to a void pointer). The mainloop will then eventually call this function and
	 provided the data pointer as an argument. Additionally, a delay can be registered
	 to the queue with the function. This delay take the form of a system time, after
	 which the work queue is allowed to execute the function. Each time mainloop finds
	 this function in the queue, it will check the current time. If the function is not
	 ready to be called, it will be added to the back of the work queue.
*/

#include "main.h"

void (*workQueue[WORK_QUEUE_LENGTH])(void *); //<! Function pointer buffer
unsigned char prodInd; //!< Producer index, the next position to write work to
unsigned char workerInd; //!< Worker index, the next position to pull work from
void * workData[WORK_QUEUE_LENGTH]; //!< Function parameter pointer buffer
long unsigned int workDelay[WORK_QUEUE_LENGTH]; //!< Delay array for corresponding functions pointers

/*!
	Adds a function pointer, data and delay to the work queue. funcName will be called
	with funcData at some time after delay.
*/
#define DO_LATER(funcName, funcData, delay) do { \
    workQueue[prodInd] = (funcName); \
    workData[prodInd] = (void *)(funcData); \
    workDelay[prodInd] = (long unsigned int)(delay); \
    INCW(prodInd); \
    } while(0)

/*!
	Increments work queue index, moves it back to the start if needed to make the queue circular.
*/
#define INCW(index) do {index++; \
    if(index >= WORK_QUEUE_LENGTH) index = 0; \
    } while(0)

/*!
   \brief Initialises board hardware and sends initial state to PC over serial.
   \post Work queue and board hardware ready for action.
*/
void do_initialization() {
    prodInd = 0;
    workerInd = 0;

    //Initialise hardware components
    init_defaults(); //for config.c
    intertial_init();
    init_command(); //init serial module
    initServo();
    initSegDisplay();
    initTimer();

    //Send range and orientation to PC every second
    send_range_to_PC(NULL);
    send_az_el_to_PC(NULL);
}

/*!
   \brief Function wrapper for DO_LATER macro, called from timer.c.
   \param func Pointer to function to schedule for later completion.
	 \param data Pointer to data to be passed to function at later time.
	 \param delay System time at which to call function, see timer.c.
   \pre do_initialization() must have been called.
   \post Task added to work queue.
*/
void call_do_later_macro(void *(func)(void*), void* data , unsigned long int delay) {
    DO_LATER(func, data, delay);
}

/*!
   \brief Main entry point, calling initial setup, then perpetually working upon
	 the work queue.
*/
void main(void) {
    extern char commandReady;

    do_initialization();
    config();   //enter onboard config mode
    EnableInterrupts;

    //enable configuraiton over serial
    enter_serial_config();

    while (1) {
        if(prodInd != workerInd) { //if there is work in the work queue
            i = getSysTime();
            if(workDelay[workerInd] > 0UL) { //if this task has a scheduled time
                if(i > workDelay[workerInd]) { //if the scheduled time for this task has passed
                    (*workQueue[workerInd])(workData[workerInd]); //do the task by calling the fucntion
                    INCW(workerInd);
                }
                else { // add the task to the back of the work queue
                    DO_LATER(workQueue[workerInd], workData[workerInd], workDelay[workerInd]);
                    INCW(workerInd);
                }
            }
            else {
                (*workQueue[workerInd])(workData[workerInd]); //else do the task
                INCW(workerInd);
            }
        }
        //if there is a serial command waiting, process it
        if(commandReady) {
            do_input_command(NULL);
        }
  	}
}
