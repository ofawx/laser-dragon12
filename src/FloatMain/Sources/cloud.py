import matplotlib
import matplotlib.pyplot as plt
import tkinter as Tk
from random import randrange
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.animation as animation
import pdb
import time
import threading
from time import strftime
import os

matplotlib.use('TkAgg')

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

#Class for containing the GUI
class Display :

    def __init__(self, plotQ, inputQ, consoleQ) :
        self.x = []
        self.y = []
        self.z = []
        self.ax = None
        self.graph = None
        self.queue = plotQ
        self.inputQ = inputQ
        self.consoleQ = consoleQ
        self.logFile = None

    #Called by matplotlib animation function every frame. This function pulls data out of the plotQ and adds it to the graph, giving a real time point cloud
    def animate(self, i) :


        if i == 0 : #initial points are used to set the axes for the graph, these must be removed
            self.x = []
            self.y = []
            self.z = []
        while True :
            if self.queue.qsize() > 0 :
                xyz = self.queue.get()
                self.x.append(xyz[0])
                self.y.append(xyz[1])
                self.z.append(xyz[2])
                if self.logFile is not None :
                    self.logFile.write(str(xyz[0]) +',' + str(xyz[1]) + ',' + str(xyz[2]) + '\n') #log these points for offline plotting
            else :
                break

        self.graph.set_data(self.y, self.z)
        self.graph.set_3d_properties(self.x)
        return self.graph

    #handle exiting the program
    def on_closing(self) :
        self.quit()
    def quit(self) :
        self.root.quit()
        self.root.destroy()
        os._exit(0)

    #Reads the consoleQ
    def console_update(self) :
        while self.consoleQ.qsize() > 0 :
            line = self.consoleQ.get()
            if line.startswith('RECIEVED: [R(') : #if we have been sent range information, display it on the GUI
                rang = int(line[line.find('(') + 1 : line.find(')')]) / 10
                self.lRange.configure(text = 'Range: ' + str(rang) + ' cm')
            elif line.startswith('RECIEVED: [AE(') : #if we have been sent azimuth, elevation information, display it on the GUI
                ab = float(line[line.find('(') + 1 : line.find(',')])
                el = float(line[line.find(',') + 2 : line.find(')')])
                self.abElev.configure(text = 'Ab: ' + str(ab) + '°, El: ' + str(el) + '°')
            elif 'BEGIN SCAN' in line : #sent if the scan is started on the board, and we need to start plotting the point cloud
                self.logFile = open(strftime('point cloud %d_%m %H_%M_%S.txt'), 'w+')
                self.x = []
                self.y = []
                self.z = []
            self.consLines.append(line)

        #display the 15 most recently recieved lines on the GUI
        self.consLines = self.consLines[-15:]
        txt = ''.join(x + '\n' for x in self.consLines)
        self.console.configure(text = txt)
        self.root.after(40, self.console_update)

    def console_add_line(self, newL) :
        self.consLines.append(newL)

    #enables the console display by updating the console
    def on_test_button(self) :
        self.console_update()

    #called when scan type selected, sends relevant command over serial
    def on_scan_type(self, t) :
        if t == 1 :
            self.consoleQ.put('Rectangle scan selected')
            self.inputQ.put('fsr_')
        else :
            self.consoleQ.put('polygon scan selected')
            self.inputQ.put('fsp_')


    def on_key_event(self, event):
        print('you pressed %s' % event.key)
        key_press_handler(event, canvas, toolbar)

    #functions called when GUI elements are interacted with, work by adding commands to inputQ to configure the board over serial

    def set_bounds(self) :
        for scale, letter in zip([self.rBound, self.lBound, self.tBound, self.bBound], ['r', 'l', 't', 'b']) :
            self.inputQ.put('fb' + letter + str(int(scale.get())) + '_')

    def on_freq_select(self, value) :
        self.consoleQ.put(str(value) + ' frequency selected')
        self.inputQ.put('ff' + str(3 ** (self.freqOptions.index(value))) + '_')

    def set_sampling_rate(self) :
        self.inputQ.put('fn' + str(int(self.lSamples.get())) + '_')

    def set_resolution(self) :
        self.inputQ.put('fr' + str(int(self.lRes.get() * 100)) + '_')


    #create a new log file and clear the plot, send the start scan command over serial
    def start_scan(self) :
        self.logFile = open(strftime('point cloud %d_%m %H_%M_%S.txt'), 'w+')
        self.x = []
        self.y = []
        self.z = []
        self.inputQ.put('s_')

    #close the log file and display a point cloud where the points are colorued according to their distance from the scanner
    def end_scan(self) :
        try:
            self.logFile.close()
            self.logFile = None
        except:
            pass
        fig = plt.figure()
        ax = fig.add_subplot(111, projection = '3d')
        ax.scatter(self.y, self.z, self.x, c = self.z, marker = '.', cmap = matplotlib.cm.jet)
        plt.show()

    #start the GUI
    def display(self) :

        points = [(-150 , -150, 0), (150, 150, 200)] #plot initial points to set the axes scale
        self.x, self.y, self.z = [a[0] for a in points], [a[1] for a in points], [a[2] for a in points]

        #create plot
        fig = plt.figure()
        self.ax = fig.add_subplot(111, projection = '3d')
        self.graph, = self.ax.plot(self.y, self.z, self.x, linestyle = "", marker=".")

        #Create GUI root
        self.root = Tk.Tk()
        frame = Tk.Frame(self.root)
        frame.pack(side = Tk.TOP, expand = 1, fill = Tk.BOTH) #create a frame and make it take up as much space as allocated by root

        #create the GUI elements
        butFrame = Tk.Frame(frame)
        butFrame.pack(side = Tk.TOP, expand = 1, fill = Tk.BOTH)

        showSerial = Tk.Button(butFrame, text = 'Show Serial', command = self.on_test_button )
        showSerial.grid(row = 0, column = 0)

        Tk.Label(butFrame, text = 'Select Object Type:').grid(row = 1, column = 0)
        radioVar = Tk.IntVar()
        Tk.Radiobutton(butFrame, text = 'Scan Rectangle', variable = radioVar, value = 1, command = lambda t = 1 : self.on_scan_type(t)).grid(row = 2, column = 0)
        Tk.Radiobutton(butFrame, text = 'Scan Polygon', variable = radioVar, value = 2, command = lambda t = 2 : self.on_scan_type(t)).grid(row = 3, column = 0)

        self.lRange = Tk.Label(butFrame, text = 'Laser Range', font = ("Consolas", 12), fg = 'red')
        self.lRange.grid(row = 4, column = 0);

        self.abElev = Tk.Label(butFrame, text = 'Abizmuth, Elevation', font = ("Consolas", 12), fg = 'green')
        self.abElev.grid(row = 4, column = 1, columnspan = 2)

        self.rBound = Tk.Scale(butFrame, label = 'Right Bound:', orient = 'horizontal', from_ = -50, to = 50)
        self.rBound.grid(row = 0, column = 1, rowspan = 2)
        self.lBound = Tk.Scale(butFrame, label = 'Left Bound:', orient = 'horizontal', from_ = -50, to = 50)
        self.lBound.grid(row = 2, column = 1, rowspan = 2)
        self.tBound = Tk.Scale(butFrame, label = 'Top Bound:', orient = 'horizontal', from_ = -25, to = 25)
        self.tBound.grid(row = 0, column = 2, rowspan = 2)
        self.bBound = Tk.Scale(butFrame, label = 'Bottom Bound:', orient = 'horizontal', from_ = -25, to = 25)
        self.bBound.grid(row = 2, column = 2, rowspan = 2)
        Tk.Button(butFrame, text = 'Set Bounds', command = self.set_bounds).grid(row = 3, column = 3)

        Tk.Label(butFrame, text = "Laser Sampling Frequency: ").grid(row = 0, column = 3)
        freqVar = Tk.StringVar(butFrame)
        freqVar.set('300Hz')
        self.freqOptions = ['300Hz', '100Hz', '33Hz', '11Hz', '3Hz', '1Hz']
        Tk.OptionMenu(butFrame, freqVar, *self.freqOptions, command = self.on_freq_select).grid(row = 1, column = 3)

        #get the matplotlib figure as a Tkinter widget, so it can be added to the GUI
        canvas = FigureCanvasTkAgg(fig, frame)
        canvas.show()
        wig = canvas.get_tk_widget().pack(side = Tk.BOTTOM, fill = Tk.BOTH, expand = 1)

        self.lSamples = Tk.Scale(butFrame, label = 'Samples/Position', orient = 'horizontal', from_ = 1, to = 10)
        self.lSamples.grid(row = 0, column = 4, columnspan = 2, rowspan = 2, padx = 3)
        Tk.Button(butFrame, text = 'Set Sampling Rate', command = self.set_sampling_rate).grid(row = 2, column = 4, columnspan = 2)

        self.lRes = Tk.Scale(butFrame, label = 'Laser Resolution', orient = 'horizontal', from_ = 0.1, to = 10, resolution = 0.1)
        self.lRes.grid(row = 0, column = 6, rowspan = 2)
        Tk.Button(butFrame, text = 'Set Resolution', command = self.set_resolution).grid(row = 2, column = 6)

        Tk.Button(butFrame, text = 'Start Scan', command = self.start_scan, padx = 5, pady = 5).grid(row = 0, column = 7, rowspan = 2)
        Tk.Button(butFrame, text = 'End Scan', command = self.end_scan, padx = 5, pady = 5).grid(row = 2, column = 7, rowspan = 2)

        #add the matplotlib toolbar to the gui
        toolbar = NavigationToolbar2TkAgg(canvas, frame)
        toolbar.update()
        canvas._tkcanvas.pack(side=Tk.TOP, fill = Tk.BOTH, expand=1)
        button = Tk.Button(master=frame, text='Quit', command=quit)
        button.pack(side=Tk.BOTTOM)

        #create the console display
        self.consLines = [' ']
        self.console = Tk.Label(frame, text = ' ', relief=Tk.SUNKEN, anchor = 'nw', height = 12, font = ('consolas', 11), fg = 'green', bg = 'black', borderwidth = 4, justify = Tk.LEFT)
        self.console.pack(fill = Tk.BOTH, expand = 1, side = Tk.BOTTOM)

        #initialise animation
        anim = animation.FuncAnimation(fig, self.animate,  interval = 100, blit=False)

        #call self.on_closing when the GUI window is closed
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)

        self.root.mainloop() #pass control over to Tkinter to run the GUI
