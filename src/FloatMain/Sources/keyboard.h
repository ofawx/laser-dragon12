/*!
	\enum Enumeration of valid keyboard keys
*/
typedef enum {INCREMENT, DECREMENT, ENTER} Key;

void init_keyboard(void); //Set port A to half input, half output for use of keyboard.
char key_select(void) ; // Returns either an 'e' for enter or 't' for toggle

Key get_next_key();
